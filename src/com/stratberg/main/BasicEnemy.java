package com.stratberg.main;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;




public class BasicEnemy extends GameObject{

	public BasicEnemy( int x, int y, ID id ){

		super( x, y, id );
		velX = 2; // up and down velocity
		velY = 3;

	}

	public Rectangle getBounds(){

		return new Rectangle( x, y, 32, 16 );
	}

	public void tick(){

		x += velX;
		y += velY;
		if ( y <= 0 || y >= Game.HEIGTH - 48 ){
			velY *= -1; // reverses direction when enemy hits screen edge
		}

		if ( x <= 0 || x >= Game.WIDTH - 16 ){
			velX *= -1;
		}
	}

	public void render( Graphics g ){
		
		g.setColor( Color.RED );
		g.fillRect( x, y, 16, 16 );
	}

	public void spawnProjectiles( char direction )
	{

		// TODO Auto-generated method stub
		
	}

}
