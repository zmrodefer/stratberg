package com.stratberg.main;


import java.awt.Graphics;
import java.awt.Rectangle;




public abstract class GameObject{
	protected int	x, y;
	protected int velMulti = 1;
	protected int powerUpTimer;
	protected ID	id;
	protected int	velX, velY;
	
	public GameObject( int x, int y, ID id ){
		this.x = x;
		this.y = y;
		this.id = id;
		
	}
	public abstract void tick();
	public abstract void render( Graphics g );
	public abstract Rectangle getBounds();
	public abstract void spawnProjectiles(char direction);
	public void setX( int x ){
		this.x = x;
	}
	public void setY( int y ){
		this.y = y;
	}
	public int getX(){
		return x;
	}
	public int getY(){
		return y;
	}
	public void setId( ID id ){
		this.id = id;
	}
	public ID getId(){
		return id;
	}
	public void setVelX( int velX ){
		this.velX = velX * velMulti;
	}
	public int getVelX(){
		return velX;
	}
	public int getVelY(){
		return velY;
	}
	public void setVelY( int velY ){
		this.velY = velY * velMulti;
	}
	public int getVelMulti(){
		return velMulti;
	}
	public void setVelMulti (int velMulti)
	{
		this.velMulti=velMulti;
	}
	public int getPowerUpTimer(){
		return powerUpTimer;
	}
	public void setPowerUpTimer (int powerUpTimer)
	{
		this.powerUpTimer=powerUpTimer;
	}
	
}
