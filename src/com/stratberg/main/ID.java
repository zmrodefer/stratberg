package com.stratberg.main;

public enum ID{
	Player(),
	BasicEnemy(),
	Trail(), 
	BasicPowerup(),
	Projectile();
}
