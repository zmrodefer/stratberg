package com.stratberg.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;


public class Projectile extends GameObject
{
	Handler	handler;
	

	public Projectile( int x, int y, ID id, char direction, Handler handler){

		super( x, y, id );  // takes variables and sets them in parent class
		this.handler = handler;
		switch (direction){
			case 'u': velY= -10;
						velX=0;
						break;
			case 'd': velY= 10;
						velX=0;
						break;
			case 'l': velX= -10;
						velY=0;
						break;
			case 'r': velX= 10;
						velY=0;	
						break;
		}
	}

	public Rectangle getBounds(){

		return new Rectangle( x, y, 8, 8 );
	}

	public void tick(){

		x += velX;
		y += velY;

		x = Game.clamp( x, 0, Game.WIDTH - 38 );
		y = Game.clamp( y, 0, Game.HEIGTH - 64 );

		collision();
		
		if ( y <= 0 || y >= Game.HEIGTH - 4 ){
			handler.removeObject(this);
		}

		if ( x <= 0 || x >= Game.WIDTH - 40 ){
			handler.removeObject(this);
		}
		

	}
	
	private void collision(){
		for ( int i = 0;i< handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			
			if (tempObject.getId() == ID.BasicEnemy && getBounds().intersects(tempObject.getBounds())){
					handler.removeObject(tempObject);
					
			}
			
			
		}
	}
	

	public void render( Graphics g ){

		g.setColor( Color.green );
		g.fillRect( x, y, 8, 8 );
	}

	public void spawnProjectiles( char direction )
	{

		// TODO Auto-generated method stub
		
	}
}
