package com.stratberg.main;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;

public class BasicPowerup extends GameObject
{
	Handler	handler;

	public BasicPowerup( int x, int y, ID id, Handler handler ){

		super( x, y, id );  // takes variables and sets them in parent class
		this.handler = handler;
	}

	public Rectangle getBounds(){

		return new Rectangle( x, y, 32, 64 );
	}

	public void tick(){

		x += velX;
		y += velY;

		x = Game.clamp( x, 0, Game.WIDTH - 38 );
		y = Game.clamp( y, 0, Game.HEIGTH - 64 );

		collision();

	}

	private void collision()
	{
		for ( int i = 0;i< handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			
			if (tempObject.getId() == ID.Player && getBounds().intersects(tempObject.getBounds())){
				handler.removeObject( this );
				
			}
		}
	}

	public void render( Graphics g )
	{

		g.setColor( Color.blue );
		g.fillRect( x, y, 32, 64 ); // bounds and rectangle fill must be equal for same hitbox
		
	}

	public void spawnProjectiles( char direction )
	{

		// TODO Auto-generated method stub
		
	}
}
