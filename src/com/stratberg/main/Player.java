package com.stratberg.main;


import java.awt.Color;
import java.awt.Graphics;
import java.awt.Rectangle;




public class Player extends GameObject{

	Handler	handler;
	

	public Player( int x, int y, ID id, Handler handler ){

		super( x, y, id );  // takes variables and sets them in parent class
		this.handler = handler;
	}

	public Rectangle getBounds(){

		return new Rectangle( x, y, 32, 32 );
	}

	public void tick(){

		x += velX;
		y += velY;

		x = Game.clamp( x, 0, Game.WIDTH - 38 );
		y = Game.clamp( y, 0, Game.HEIGTH - 64 );

		collision();
		
		if(powerUpTimer > 0){
			powerUpTimer--;
		}
		if (powerUpTimer == 0)
		{
			velMulti = 1;
		}

	}
	
	private void collision(){
		for ( int i = 0;i< handler.object.size(); i++){
			GameObject tempObject = handler.object.get(i);
			
			if (tempObject.getId() == ID.BasicEnemy && getBounds().intersects(tempObject.getBounds())){
					HUD.HEALTH -= 2;
			}
			else if (tempObject.getId() == ID.BasicPowerup && getBounds().intersects(tempObject.getBounds()))
			{
				powerUpTimer=600;
				velMulti=2;
			}
			
		}
	}
	public void spawnProjectiles(char direction)
	{
		switch (direction)
		{
			case 'u': handler.addObject( new Projectile( x, y, ID.Projectile, 'u', handler ) );
			break;
			case 'd': handler.addObject( new Projectile( x, y, ID.Projectile, 'd', handler ) );
			break;
			case 'l': handler.addObject( new Projectile( x, y, ID.Projectile, 'l', handler ) );
			break;
			case 'r': handler.addObject( new Projectile( x, y, ID.Projectile, 'r', handler ) );
			break;
			
			
		}
	}

	public void render( Graphics g ){

		g.setColor( Color.white );
		g.fillRect( x, y, 32, 32 );
	}
}
